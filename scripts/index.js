
/*
----- [ теория ] -----
1) функции используются для того чтобы "сгруппировать" код, который повторяется, и,
 при потребности, вызывать его из разных частей программы, изменяя только передаваемые значения.
2) аргументы передаются в функцию для того чтобы провести с ними какие-либо локальные операции, 
   после чего можно было бы получить результат этих операций, вызвав функцию с заданным ранее количеством аргументов
   и задав ей столько же значений
3) return - оператор, который служит для прекращения выполнения функции (если прописать только return) 
   и для возврата значения выражения, которое ему задается, в место, где вызывается функция.

   Как только функция доходит до оператора return, она принимает значение его выражения/операции в себя
   и возвращает в место вызова.
*/


let randomNumber = "";
while (randomNumber === null || randomNumber === "" || isNaN(randomNumber)) {
    randomNumber = prompt("enter the first number:", `${randomNumber}`);
}
    
let randomNumberSecond = "";
    while (randomNumberSecond === null || randomNumberSecond === "" || isNaN(randomNumberSecond)) {
    randomNumberSecond = prompt("enter the second number:", `${randomNumberSecond}`);
}

let randomOperation = prompt("enter a mathematical operation:")
if ((randomOperation === "+") || (randomOperation === "-") || (randomOperation === "*") || (randomOperation === "/")) {

    function calcForMathOperations(randomNumber, randomNumberSecond) {

        if (randomOperation === "+") {
            return +randomNumber + +randomNumberSecond;
        } else if (randomOperation === "-") {
            return randomNumber - randomNumberSecond;
        } else if (randomOperation === "*") {
            return randomNumber * randomNumberSecond;
        } else if (randomOperation === "/") {
            return randomNumber / randomNumberSecond;
        }
    }

    console.log(calcForMathOperations(randomNumber, randomNumberSecond))
} else {
    alert("it is not a mathematical operation or there is no such mathematical operation.") 
}
//у меня почему-то не получается сделать цикл для повтора 3 модалки. 
// писала while ((randomOperation != "+") || (randomOperation != "-") || (randomOperation != "*") || (randomOperation != "/")){randomOperation = prompt("enter a mathematical operation: ")}